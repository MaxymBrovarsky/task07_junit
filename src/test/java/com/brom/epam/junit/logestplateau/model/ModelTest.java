package com.brom.epam.junit.logestplateau.model;

import com.brom.epam.junit.longestplateau.model.Model;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ModelTest {
  @Test
  public void getValuesTest() throws NoSuchFieldException, IllegalAccessException {
    List<Integer> values = new ArrayList<>();
    values.add(3);
    values.add(5);
    Model model = new Model();
    final Field field = Model.class.getDeclaredField("values");
    field.setAccessible(true);
    field.set(model, values);
    TestCase.assertEquals(values, model.getValues());
  }
  @Test
  public void setValuesTest() throws NoSuchFieldException, IllegalAccessException {
    List<Integer> values = new ArrayList<>();
    values.add(3);
    values.add(5);
    Model model = new Model();
    model.setValues(values);
    final Field field = Model.class.getDeclaredField("values");
    field.setAccessible(true);
    TestCase.assertEquals(values, field.get(model));
  }
}
