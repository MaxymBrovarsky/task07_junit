package com.brom.epam.junit.logestplateau.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import com.brom.epam.junit.longestplateau.controller.Controller;
import com.brom.epam.junit.longestplateau.model.LongestPlateau;
import com.brom.epam.junit.longestplateau.model.Model;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTests {
  @Mock
  Model model;

  @InjectMocks
  Controller controller = new Controller();

  @Test
  public void testFindTheLongestPlateau() {
    List<Integer> values = Arrays.asList(1, 3, 3, 3, 1);
    when(model.getValues()).thenReturn(values);
    Optional<LongestPlateau> longestPlateauOptional = controller.findTheLongestPlateau();
    LongestPlateau longestPlateau = longestPlateauOptional.get();
    TestCase.assertTrue(longestPlateau.getLength() == 3 && longestPlateau.getLocation() == 1);
  }

  @Test
  public void testSaveValues() {
    List<Integer> values = Arrays.asList(1, 3, 3, 3, 1);
    controller.saveValues(values);
    verify(model).setValues(values);
  }

}
