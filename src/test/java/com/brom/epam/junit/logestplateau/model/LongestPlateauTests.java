package com.brom.epam.junit.logestplateau.model;

import static junit.framework.TestCase.assertTrue;

import com.brom.epam.junit.longestplateau.model.LongestPlateau;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized;

public class LongestPlateauTests {
  @Test
  public void constructorTest() {
    int lengthValue = 10;
    int location = 1;
    LongestPlateau longestPlateau = new LongestPlateau(lengthValue, location);
    assertTrue(longestPlateau.getLength() == lengthValue &&
        longestPlateau.getLocation() == location);
  }

  @ParameterizedTest
  @MethodSource("getConstructorBadParametersTestData")
  public void testConstructorBadParametersValues(int length, int location) {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> new LongestPlateau(length, location));
  }

  @Parameterized.Parameters
  public static Stream<Arguments> getConstructorBadParametersTestData() {
    return Stream.of(
        Arguments.of(-1, 0),
        Arguments.of(0, 1),
        Arguments.of(0, 0),
        Arguments.of(1, -1)
    );
  }
}
