package com.brom.epam.junit.minesweeper.model;

import java.lang.reflect.Field;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class GameFieldTests {
  private GameField gameField;
  private boolean[][] field;
  private String rawRepresentation;
  private String numberRepresentation;
  @Before
  public void beforeAll() {
    field = new boolean[][]{{true, false, true}, {false, true, false}, {true, false, true}};
    gameField = new GameField(field);
    rawRepresentation = "*.*\n.*.\n*.*\n";
    numberRepresentation = "*3*\n3*3\n*3*\n";
  }
  @Test
  public void testConstructor() throws NoSuchFieldException, IllegalAccessException {
    Field field = gameField.getClass().getDeclaredField("field");
    field.setAccessible(true);
    boolean[][] fieldValue = (boolean[][]) field.get(gameField);
    TestCase.assertTrue(fieldValue.length == 5 && fieldValue[0].length == 5);
  }

  @Test
  public void testFieldBorder() throws NoSuchFieldException, IllegalAccessException {
    Field field = gameField.getClass().getDeclaredField("field");
    field.setAccessible(true);
    boolean[][] fieldValue = (boolean[][]) field.get(gameField);
    for (int i = 0; i < fieldValue.length; i++) {
      for (int j = 0; j < fieldValue[i].length; j++) {
        if (i  == 0 || i == fieldValue.length - 1) {
          if (fieldValue[i][j]) {
            TestCase.fail();
          }
        } else {
          if (j == 0 || j == fieldValue[i].length - 1) {
            if (fieldValue[i][j]) {
              TestCase.fail();
            }
          }
        }
      }
    }
  }

  @Test
  public void testRawRepresentation() {
    TestCase.assertEquals(rawRepresentation, gameField.getRawRepresentation());
  }
  @Test
  public void testNumberRepresentation() {
    TestCase.assertEquals(numberRepresentation, gameField.getPresentationWithNumbers());
  }
}
