package com.brom.epam.junit.minesweeper.controller;


import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.brom.epam.junit.minesweeper.model.GameField;
import java.lang.reflect.Field;
import java.util.Random;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MinesweeperControllerTests {
  @Mock
  GameField model;

  @Mock
  Random random;

  @InjectMocks
  MinesweeperController controller = new MinesweeperControllerImpl();

  @Test
  public void testInitField() {
    controller.initField(3,3,25);
    verify(random, times(9)).nextInt(100);
  }

  @Test
  public void testGetField() throws NoSuchFieldException, IllegalAccessException {
    Field field = controller.getClass().getDeclaredField("gameField");
    field.setAccessible(true);
    GameField gameField = (GameField) field.get(controller);
    TestCase.assertEquals(controller.getField(), gameField);
  }
}
