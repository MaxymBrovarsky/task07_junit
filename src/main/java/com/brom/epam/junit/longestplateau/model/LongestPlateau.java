package com.brom.epam.junit.longestplateau.model;

public class LongestPlateau {
  private int length;
  private int location;

  public LongestPlateau(int length, int location) throws IllegalArgumentException {
    if (length <= 0 || location <= 0) {
      throw new IllegalArgumentException();
    }
    this.length = length;
    this.location = location;
  }

  public int getLength() {
    return length;
  }

  public int getLocation() {
    return location;
  }
}
