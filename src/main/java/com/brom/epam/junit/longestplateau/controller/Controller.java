package com.brom.epam.junit.longestplateau.controller;

import com.brom.epam.junit.longestplateau.model.LongestPlateau;
import com.brom.epam.junit.longestplateau.model.Model;
import java.util.List;
import java.util.Optional;

public class Controller {
  private Model model;

  public Controller() {
    this.model = new Model();
  }
  public void saveValues(List<Integer> values) {
    this.model.setValues(values);
  }

  public Optional<LongestPlateau> findTheLongestPlateau() {
    List<Integer> values = this.model.getValues();
    int longestPlateauLength = 0;
    int longestPlateauStartIndex = -1;
    for (int i = 0; i < values.size() - 1; i++) {
      if (values.get(i) < values.get(i + 1)) {
        int currentPlateauLength = 1;
        while (i + 1 + currentPlateauLength < values.size() && values.get(i + 1).equals(values.get(i + 1 + currentPlateauLength))) {
          currentPlateauLength++;
        }
        if (i + 1 + currentPlateauLength < values.size() && values.get(i + 1) > values.get(i + 1 +currentPlateauLength)) {
          if (longestPlateauLength < currentPlateauLength) {
            longestPlateauLength = currentPlateauLength;
            longestPlateauStartIndex = i + 1;
          }
        }
      }
    }
    if (longestPlateauLength == 0) {
      return Optional.empty();
    }
    return Optional.of(new LongestPlateau(longestPlateauLength, longestPlateauStartIndex));
  }
}
