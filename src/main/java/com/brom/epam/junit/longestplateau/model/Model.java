package com.brom.epam.junit.longestplateau.model;

import java.util.List;

public class Model {
  private List<Integer> values;

  public void setValues(List<Integer> values) {
    this.values = values;
  }

  public List<Integer> getValues() {
    return values;
  }
}
