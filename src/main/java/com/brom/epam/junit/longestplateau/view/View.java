package com.brom.epam.junit.longestplateau.view;

import com.brom.epam.junit.longestplateau.controller.Controller;
import com.brom.epam.junit.longestplateau.model.LongestPlateau;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private Logger logger = LogManager.getLogger(View.class.getName());
  private Scanner input = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Command> menuCommands;
  private Controller controller;

  public View() {
    this.controller = new Controller();
    initMenu();
    initMenuCommands();
  }

  private void initMenu() {
    this.menu = new HashMap<>();
    this.menu.put("1", "1. Enter values of array");
    this.menu.put("2", "2. Find the longest plateau");
    this.menu.put("3", "3. Quit");
  }

  private void initMenuCommands() {
    this.menuCommands = new HashMap<>();
    this.menuCommands.put("1", this::enterValues);
    this.menuCommands.put("2", this::findTheLongestPlateau);
    this.menuCommands.put("3", this::quit);
  }

  private void enterValues() {
    logger.info("Please enter values (end input with empty line)");
    List<Integer> values = new ArrayList<>();
    while (true) {
      try {
        String value = input.nextLine();
        if (value.isEmpty()) {
          break;
        }
        values.add(Integer.parseInt(value));
      } catch (NumberFormatException e) {
        logger.error("You've entered wrong value, try again");
      }
    }
    controller.saveValues(values);
  }

  private void findTheLongestPlateau() {
    Optional<LongestPlateau> longestPlateau = controller.findTheLongestPlateau();
    if (longestPlateau.isPresent()) {
      LongestPlateau plateau = longestPlateau.get();
      logger.info("Plateau:");
      logger.info("length = " + plateau.getLength() + " start index = " + plateau.getLocation());
    } else {
      logger.info("Here no plateau");
    }
  }

  private void quit() {
    System.exit(0);
  }

  public void show() {
    while (true) {
      menu.values().forEach(v -> logger.info(v));
        try {
          String commandKey = input.nextLine();
          menuCommands.get(commandKey).execute();
        } catch (NullPointerException e) {
          logger.error("bad command");
        }

    }
  }
}
