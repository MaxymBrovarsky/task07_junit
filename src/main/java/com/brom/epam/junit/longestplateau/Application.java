package com.brom.epam.junit.longestplateau;

import com.brom.epam.junit.longestplateau.view.View;

public class Application {
  public static void main(String[] args) {
    new View().show();
  }
}
