package com.brom.epam.junit.longestplateau.view;

public interface Command {
  void execute();
}
