package com.brom.epam.junit.minesweeper.controller;

import com.brom.epam.junit.minesweeper.model.GameField;
import java.util.Random;

public class MinesweeperControllerImpl implements MinesweeperController {
  private GameField gameField;
  private Random r;

  public MinesweeperControllerImpl() {
    r = new Random();
  }

  @Override
  public void initField(int height, int width, int probabilityInPercents) {
    boolean[][] field = createField(height, width, probabilityInPercents);
    this.gameField = new GameField(field);
  }

  private boolean[][] createField(int height, int width, int probabilityInPercents) {
    boolean[][] field = new boolean[height][width];
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        int randomValue = r.nextInt(100);
        if (randomValue < probabilityInPercents) {
          field[i][j] = true;
        }
      }
    }
    return field;
  }

  @Override
  public GameField getField() {
    return gameField;
  }
}
