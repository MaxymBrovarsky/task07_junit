package com.brom.epam.junit.minesweeper.view;

public interface Command {
  void execute();
}
