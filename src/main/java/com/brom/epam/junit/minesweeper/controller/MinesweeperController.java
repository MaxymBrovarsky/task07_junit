package com.brom.epam.junit.minesweeper.controller;

import com.brom.epam.junit.minesweeper.model.GameField;

public interface MinesweeperController {
  void initField(int height, int width, int probabilityInPercents);
  GameField getField();
}
