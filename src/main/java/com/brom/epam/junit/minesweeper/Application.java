package com.brom.epam.junit.minesweeper;

import com.brom.epam.junit.minesweeper.controller.MinesweeperControllerImpl;
import com.brom.epam.junit.minesweeper.view.View;

public class Application {
  public static void main(String[] args) {
    new View(new MinesweeperControllerImpl()).show();
  }
}
