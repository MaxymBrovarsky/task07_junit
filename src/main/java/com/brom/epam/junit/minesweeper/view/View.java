package com.brom.epam.junit.minesweeper.view;

import com.brom.epam.junit.minesweeper.controller.MinesweeperController;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private Logger logger = LogManager.getLogger(View.class.getName());
  private Scanner input = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Command> menuCommands;
  private MinesweeperController controller;

  public View(MinesweeperController controller) {
    this.controller = controller;
    initMenu();
    initMenuCommands();
  }

  private void initMenu() {
    menu = new LinkedHashMap<>();
    menu.put("1", "1. Init field");
    menu.put("2", "2. Print field");
    menu.put("3", "3. Print solution");
    menu.put("4", "4. Quit");
  }

  private void initMenuCommands() {
    menuCommands = new LinkedHashMap<>();
    menuCommands.put("1", this::initField);
    menuCommands.put("2", this::printField);
    menuCommands.put("3", this::printSolution);
    menuCommands.put("4", this::quit);
  }

  private void initField() {
    logger.info("Enter height of field");
    int height = input.nextInt();
    input.nextLine();
    logger.info("Enter width of field");
    int width = input.nextInt();
    input.nextLine();
    logger.info("Probability of bomb spawn(in percents)");
    int probability = input.nextInt();
    input.nextLine();
    controller.initField(height, width, probability);
  }

  private void printField() {
    logger.info(this.controller.getField().getRawRepresentation());
  }

  private void printSolution() {
    logger.info(this.controller.getField().getPresentationWithNumbers());
  }

  private void quit() {
    System.exit(0);
  }

  public void show() {
    while (true) {
      menu.values().forEach(v -> logger.info(v));
      try {
        String commandKey = input.nextLine();
        menuCommands.get(commandKey).execute();
      } catch (NullPointerException e) {
        logger.error("bad command");
      }
    }
  }
}
