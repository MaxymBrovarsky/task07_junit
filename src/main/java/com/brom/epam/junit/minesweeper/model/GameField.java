package com.brom.epam.junit.minesweeper.model;

public class GameField {
  private boolean[][] field;

  public GameField(boolean[][] field) {
    this.field  = new boolean[field.length + 2][field[0].length + 2];
    for (int i = 0; i < field.length; i++) {
      for (int j = 0; j < field[i].length; j++) {
        this.field[i + 1][j + 1] = field[i][j];
      }
    }
  }

  public String getRawRepresentation() {
    StringBuilder sb = new StringBuilder();
    for (int i = 1; i < field.length - 1; i++) {
      for (int j = 1; j < field[i].length - 1; j++) {
        if (field[i][j]) {
          sb.append('*');
        } else {
          sb.append('.');
        }
      }
      sb.append("\n");
    }
    return sb.toString();
  }
  
  public String getPresentationWithNumbers() {
    StringBuilder sb = new StringBuilder();
    for (int i = 1; i < field.length - 1; i++) {
      for (int j = 1; j < field[i].length - 1; j++) {
        if (field[i][j]) {
          sb.append('*');
        } else {
          sb.append(this.countMinesAround(i, j));
        }
      }
      sb.append("\n");
    }
    return sb.toString();
  }

  private int countMinesAround(int row, int column) {
    int minesCount = 0;
    for (int i = row - 1; i <= row + 1; i++) {
      for (int j = column - 1; j <= column + 1; j++) {
        if (field[i][j]) {
          minesCount++;
        }
      }
    }
    return minesCount;
  }
}
